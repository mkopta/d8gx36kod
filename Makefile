CC        = cc
CFLAGS    = -ansi
CFLAGS   += -Wall -Wextra -Wconversion -pedantic
#CFLAGS   += -DDEBUG -ggdb3
CLIBS     =
OBJS      = list.o d8gx36kod.o

all: d8gx36kod

d8gx36kod: $(OBJS) list.h
	$(CC) $(CFLAGS) $(CLIBS) -o d8gx36kod $(OBJS)

list.o: list.h
	$(CC) $(CFLAGS) $(CLIBS) -c -o list.o list.c

d8gx36kod.o: list.h
	$(CC) $(CFLAGS) $(CLIBS) -c -o d8gx36kod.o d8gx36kod.c

run:
	./d8gx36kod

clean:
	rm -f d8gx36kod $(OBJS)
