/* Copyright (c) <2010> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************  }}}1*/

#include <stdio.h>
#include <stdlib.h>
#include "./list.h"

static void abort_with_msg(const char *c) {
  fprintf(stderr, "%s\n", c);
  abort();
}

unsigned power(unsigned base, unsigned exp) {
  unsigned result = 1, i;
  for (i = 0; i < exp; i++)
    result *= base;
  return result;
}

void print_code(List *code) {
  Node *n = code->head;
  if (list_empty(code))
    return;
  do {
    putchar(n->data ? '1' : '0');
  } while ((n = n->next) != NULL);
  putchar('\n');
}

unsigned alpha_encode(List *code, unsigned num) {
  unsigned i;
  list_purge(code);
  for (i = 1; i < num; i++) {
    list_push(code, 0);
  }
  list_push(code, 1);
  return i;
}

unsigned alpha_decode(List *code) {
  unsigned num = 0;
  Node *n = code->head;
  while (n != NULL && n->data != 1) {
    num++;
    n = n->next;
  }
  return ++num;
}

unsigned beta_encode(List *code, unsigned num) {
  unsigned i = 0;
  list_purge(code);
  while (num != 0) {
    list_unshift(code, (unsigned) (num & 1));
    num = num >> 1;
    i++;
  }
  return i;
}

unsigned beta_decode(List *code) {
  unsigned num = 0, i = 0;
  Node *n = code->tail;
  while (n != NULL) {
    if (n->data == 1)
      num += power(2, i);
    n = n->prev;
    i++;
  }
  return num;
}

unsigned beta2_encode(List *code, unsigned num) {
  unsigned size;
  list_purge(code);
  size = beta_encode(code, num);
  (void) list_shift(code);
  return --size;
}

unsigned beta2_decode(List *code) {
  list_unshift(code, 1);
  return beta_decode(code);
}

unsigned gamma_encode(List *code, unsigned num) {
  unsigned beta_code_size, gamma_code_size = 0;
  List *l1 = list_create();
  List *l2 = list_create();
  list_purge(code);
  beta_code_size = beta_encode(l1, num);
  (void) list_shift(l1); /* beta2 */
  (void) alpha_encode(l2, beta_code_size);
  while (!list_empty(l2)) {
    list_push(code, list_shift(l2));
    gamma_code_size++;
    if (!list_empty(l1)) {
      list_push(code, list_shift(l1));
      gamma_code_size++;
    }
  }
  list_destroy(l2);
  list_destroy(l1);
  return gamma_code_size;
}

unsigned gamma_decode(List *code) {
  unsigned num = 1;
  List *l = list_create();
  Node *n = code->head;
GAMMA_DECODE_LOOP:
  if (n == NULL) abort_with_msg("gamma_decode(), invalid code");
  if (n->data == 1) {
    goto GAMMA_DECODE_END;
  } else if (n->data == 0) {
    n = n->next;
    if (n == NULL) abort_with_msg("gamma_decode(), invalid code");
    list_push(l, n->data);
    n = n->next;
    if (n == NULL) abort_with_msg("gamma_decode(), invalid code");
  }
  goto GAMMA_DECODE_LOOP;
GAMMA_DECODE_END:
  if (!list_empty(l))
    num = beta2_decode(l);
  list_destroy(l);
  return num;
}

unsigned gamma2_encode(List *code, unsigned num) {
  unsigned beta_code_size, gamma2_code_size = 0;
  List *alpha_code = list_create();
  List *beta_code = list_create();
  list_purge(code);
  beta_code_size = beta_encode(beta_code, num);
  (void) alpha_encode(alpha_code, beta_code_size);
  (void) list_shift(beta_code); /* beta2 */
  while (!list_empty(alpha_code)) {
    list_push(code, list_shift(alpha_code));
    gamma2_code_size++;
  }
  while (!list_empty(beta_code)) {
    list_push(code, list_shift(beta_code));
    gamma2_code_size++;
  }
  list_destroy(alpha_code);
  list_destroy(beta_code);
  return gamma2_code_size;
}

unsigned gamma2_decode(List *code) {
  unsigned beta_code_length = 1, num;
  Node *n = code->head;
  List *beta2_code = list_create();
  if (n == NULL) abort_with_msg("gamma2_decode(), invalid code");
  while (n->data != 1) {
    beta_code_length++;
    n = n->next;
    if (n == NULL) abort_with_msg("gamma2_decode(), invalid code");
  }
  while (--beta_code_length) {
    n = n->next;
    if (n == NULL) abort_with_msg("gamma2_decode(), invalid code");
    list_push(beta2_code, n->data);
  }
  num = beta2_decode(beta2_code);
  list_destroy(beta2_code);
  return num;
}


unsigned delta_encode(List *code, unsigned num) {
  unsigned gamma_code_size, delta_code_size = 0, beta_code_size;
  List *beta_code = list_create();
  List *gamma_code = list_create();
  beta_code_size = beta_encode(beta_code, num);
  (void) list_shift(beta_code); /* beta2 */
  gamma_code_size = gamma_encode(gamma_code, beta_code_size);
  list_purge(code);
  while (!list_empty(gamma_code)) {
    list_push(code, list_shift(gamma_code));
    delta_code_size++;
  }
  while (!list_empty(beta_code)) {
    list_push(code, list_shift(beta_code));
    delta_code_size++;
  }
  list_destroy(gamma_code);
  list_destroy(beta_code);
  return delta_code_size;
}

unsigned delta_decode(List *code) {
  unsigned num = 1, beta_code_length, beta2_code_length;
  List *gamma_code = list_create();
  List *beta2_code = list_create();
  Node *n = code->head;
  if (n == NULL) abort_with_msg("delta_decode(), invalid code");
  while (n->data != 1) {
    list_push(gamma_code, n->data);
    n = n->next;
    if (n == NULL) abort_with_msg("delta_decode(), invalid code");
    list_push(gamma_code, n->data);
    n = n->next;
    if (n == NULL) abort_with_msg("delta_decode(), invalid code");
  }
  list_push(gamma_code, n->data);
  printf("Gamma: ");
  print_code(gamma_code);
  beta_code_length = gamma_decode(gamma_code);
  if (beta_code_length > 0) {
    beta2_code_length = beta_code_length - 1;
    printf("Beta2 length: %u\n", beta2_code_length);
    while (beta2_code_length--) {
      n = n->next;
      if (n == NULL) abort_with_msg("delta_decode(), invalid code");
      list_push(beta2_code, n->data);
    }
    num = beta2_decode(beta2_code);
  }
  list_destroy(beta2_code);
  list_destroy(gamma_code);
  return num;
}

/* ternary comma code
   00   0
   01   1
   10   2
   11   c
 */
unsigned tcc_encode(List *code, unsigned num) {
  unsigned tcc_code_length = 0, a, b;
  if (num == 0) abort_with_msg("tcc_encode(), cannot encode zero");
  if (num == 1) goto TCC_ENCODE_CONVERSION_LOOP_END;
  list_purge(code);
  num -= 2;
TCC_ENCODE_CONVERSION_LOOP_BEGIN:
  a = num / 3;
  b = num % 3;
  switch (b) {
    case 0:
      list_unshift(code, 0);
      list_unshift(code, 0);
      break;
    case 1:
      list_unshift(code, 1);
      list_unshift(code, 0);
      break;
    case 2:
      list_unshift(code, 0);
      list_unshift(code, 1);
      break;
  }
  tcc_code_length += 2;
  if (a == 0) goto TCC_ENCODE_CONVERSION_LOOP_END;
  num = a;
  goto TCC_ENCODE_CONVERSION_LOOP_BEGIN;
TCC_ENCODE_CONVERSION_LOOP_END:
  /* pushing comma (= 11) */
  list_push(code, 1);
  list_push(code, 1);
  return tcc_code_length + 2;
}

unsigned tcc_decode(List *code) {
  unsigned num = 0, exp = 0;
  int x, y;
  List *ternary_code = list_create();
TCC_DECODE_LOOP_BEGIN:
  if (list_empty(code)) abort_with_msg("tcc_decode(), invalid code [1]");
  x = list_shift(code);
  if (list_empty(code)) abort_with_msg("tcc_decode(), invalid code [2]");
  y = list_shift(code);
  if (x == 1 && y == 1) goto TCC_DECODE_LOOP_END;
  list_push(ternary_code, x);
  list_push(ternary_code, y);
  goto TCC_DECODE_LOOP_BEGIN;
TCC_DECODE_LOOP_END:
  if (list_empty(ternary_code)) return 1U;
  while (!list_empty(ternary_code)) {
    y = list_pop(ternary_code);
    if (list_empty(ternary_code)) abort_with_msg("tcc_decode(), invalid code [3]");
    x = list_pop(ternary_code);
    if (x == 0 && y == 0) {
      /* num += 0 * power(3, exp); */
    } else if (x == 0 && y == 1) {
      num += 1 * power(3, exp);
    } else if (x == 1) {
      num += 2 * power(3, exp);
    } else {
      abort_with_msg("tcc_decode(), invalid code [4]");
    }
    exp++;
  }
  list_destroy(ternary_code);
  return num + 2;
}

unsigned omega_encode(List *code, unsigned num) {
  unsigned size = 1;
  List *l = list_create();
  list_unshift(code, 0);
  if (num == 1) return size;
  num++;
  do {
    num = beta_encode(l, num - 1);
    size += num;
    while (!list_empty(l))
      list_unshift(code, list_pop(l));
  } while (num > 2);
  list_destroy(l);
  return size;
}

unsigned omega_decode(List *code) {
  unsigned last = 1;
  int i;
  List *l = list_create();
  Node *n = code->head;
  if (n == NULL) abort_with_msg("omega_decode(), invalid code [1]");
  while (n->data != 0) {
    list_push(l, n->data);
    for (i = (int) last; i > 0; i--) {
      n = n->next;
      if (n == NULL) abort_with_msg("omega_decode(), invalid code [2]");
      list_push(l, n->data);
    }
    last = beta_decode(l);
    list_purge(l);
    n = n->next;
    if (n == NULL) abort_with_msg("omega_decode(), invalid code [3]");
  }
  list_destroy(l);
  return last;
}

int main(void) {
  unsigned i = 1;
  List *code = list_create();
  for (; i < 100; i++) {
    printf("Original: %u\n", i);
    printf("Length: %u\nCode: ", omega_encode(code, i));
    print_code(code);
    printf("Original: %u\n\n", omega_decode(code));
  }
  list_destroy(code);
  return 0;
}
