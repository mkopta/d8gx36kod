/* Copyright (c) <2010> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************  }}}1*/

#ifndef _list_h_
#define _list_h_

typedef struct node {
  int data;
  struct node *prev;
  struct node *next;
} Node;

typedef struct {
  Node *head;
  Node *tail;
  unsigned size;
} List;

List *list_create(void);
void list_destroy(List *l);

/* pop from end of the list */
int list_pop(List *l);
/* push to the end of the list */
void list_push(List *l, int data);

/* take from begin of the list */
int list_shift(List *l);
/* insert at begin of the list */
void list_unshift(List *l, int data);

/* insert data at position */
void list_insert(List *l, unsigned position, int data);

unsigned list_size(List *l);
int list_empty(List *l);

int list_find(List *l, int data);
int list_at(List *l, unsigned position);

void list_purge(List *l);

#endif /* _list_h_ */
