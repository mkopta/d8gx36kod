/* Copyright (c) <2010> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************  }}}1*/

#include <stdio.h>
#include <stdlib.h>
#include "./list.h"

void list_destroy(List *l) {
  while (!list_empty(l))
    (void) list_pop(l);
  free(l);
}

List *list_create(void) {
  List *l = (List *) malloc(sizeof(List));
  if (l == NULL) {
    perror("malloc");
    abort();
  } else {
    l->tail = NULL;
    l->head = NULL;
    l->size = 0;
  }
  return l;
}

int list_pop(List *l) {
  int data = 0;
  Node *n = l->tail;
  if (l->size == 0) {
    fprintf(stderr, "ERROR: list_pop(), empty list\n");
    abort();
  }
  if (n->prev == NULL) {
    l->head = NULL;
    l->tail = NULL;
    l->size = 0;
  } else {
    l->tail = l->tail->prev;
    l->tail->next = NULL;
    l->size--;
  }
  data = n->data;
  free(n);
  return data;
}

void list_push(List *l, int data) {
  Node *n = (Node *) malloc(sizeof(Node));
  if (n == NULL) {
    perror("malloc");
    abort();
  }
  n->prev = NULL;
  n->next = NULL;
  n->data = data;
  if (l->size == 0) {
    l->head = n;
    l->tail = n;
    l->size = 1;
  } else {
    l->tail->next = n;
    n->prev = l->tail;
    l->tail = n;
    l->size++;
  }
}

int list_shift(List *l) {
  int data = 0;
  Node *n = l->head;
  if (l->size == 0) {
    fprintf(stderr, "ERROR: list_shift(), empty list\n");
    abort();
  } else if (l->head->next == NULL) {
    l->head = NULL;
    l->tail = NULL;
    l->size = 0;
  } else {
    l->head = l->head->next;
    l->head->prev = NULL;
    l->size--;
  }
  data = n->data;
  free(n);
  return data;
}

void list_unshift(List *l, int data) {
  Node *n = (Node *) malloc(sizeof(Node));
  if (n == NULL) {
    perror("malloc");
    abort();
  }
  n->prev = NULL;
  n->next = NULL;
  n->data = data;
  if (l->size == 0) {
    l->head = n;
    l->tail = n;
    l->size = 1;
  } else {
    n->prev = NULL;
    n->next = l->head;
    l->head->prev = n;
    l->head = n;
    l->size++;
  }
}

unsigned list_size(List *l) {
  return l->size;
}

int list_empty(List *l) {
  return l->size == 0;
}

int list_at(List *l, unsigned position) {
  Node *n = l->head;
  if (position >= l->size) {
    fprintf(stderr, "ERROR: list_at(), out of bounds\n");
    abort();
  } else {
    while (position--) {
      n = n->next;
    }
    return n->data;
  }
}

int list_find(List *l, int data) {
  int position = 0;
  Node *n = l->head;
  while (n != NULL) {
    if (n->data == data) {
      return position;
    } else {
      n = n->next;
      position++;
    }
  }
  return -1;
}

void list_insert(List *l, unsigned position, int data) {
  Node *n = l->head;
  if (position >= l->size) {
    fprintf(stderr, "ERROR: list_insert(), out of bounds\n");
    abort();
  } else {
    while (position--) {
      n = n->next;
    }
    n->data = data;
  }
}

void list_purge(List *l) {
  while (!list_empty(l))
    list_pop(l);
}
